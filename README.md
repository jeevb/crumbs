# cRumbs

A collection of my most frequently used R code, packaged into human-friendly wrappers.

##### Installation:
```
## Install the required packages from Bioconductor
source('http://bioconductor.org/biocLite.R')
biocLite(c('Biobase', 'edgeR', 'GenomicRanges', 'KEGGREST', 'limma', impute', 'preprocessCore', 'GO.db', 'AnnotationDbi', 'org.Hs.eg.db', 'org.Mm.eg.db', 'minet'))

## Install the required R packages from CRAN
install.packages(c('cluster', 'dplyr', 'gplots', 'plyr', 'RColorBrewer', 'WGCNA'))

## Install Hadley Wickham's devtools package
install.packages('devtools')

## Install cRumbs
devtools::install_bitbucket('jeevb/cRumbs')
```
