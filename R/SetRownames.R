SetRownames <- function(df, col, remove = TRUE) {
    if (is.character(col))
        col <- which(names(df) == col)
    rownames(df) <- make.names(df[, col], unique = TRUE)

    if (remove)
        df <- df[, -col]

    df
}
