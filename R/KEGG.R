KEGG <- function(type = 'GENE', species = 'hsa') {
    pathways <- KEGGREST::keggList('pathway', species)
    pathway.ids <- gsub('path:', '', names(pathways))
    pathway.names <- gsub('(.*) - .+$', '\\1', pathways)

    structure(
        lapply(pathway.ids, function(id) {
            members <- KEGGREST::keggGet(id)[[1]][[type]]
            if (type == 'GENE') members <- members[grepl('^\\d+$', members)]
            if (type == 'COMPOUND') members <- names(members)

            members
        }),
        names=paste(pathway.ids, pathway.names, sep='|')
        )
}
