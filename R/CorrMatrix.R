setGeneric('FilterZeroVariance',
    function(x, transpose = TRUE) {
        standardGeneric('FilterZeroVariance')
    }
    )
setMethod('FilterZeroVariance',
    signature = signature(x = 'matrix'),
    function(x, transpose) {
        ## Transpose if necessary
        ## Plot pairwise correlations of genes if transposed
        ## Plot pairwise correlations of samples if not transposed
        dat <- x
        if (transpose)
            dat <- t(dat)

        ## Remove columns with no variance
        bad <- apply(dat, 2, var, na.rm = TRUE) == 0
        dat[, !bad]
    }
    )

setGeneric('CorrCoeffs',
    function(x, transpose = TRUE, use = 'everything', method = 'pearson') {
        standardGeneric('CorrCoeffs')
    }
    )
setMethod('CorrCoeffs',
    signature = signature(x = 'matrix'),
    function(x, transpose, use, method) {
        use <- match.arg(use,
                         c('all.obs',
                           'complete.obs',
                           'pairwise.complete.obs',
                           'everything',
                           'na.or.complete'))
        method <- match.arg(method, c('pearson', 'kendall', 'spearman'))

        dat <- FilterZeroVariance(x, transpose = transpose)
        cor(dat, use = use, method = method)
    }
    )

setMethod('CorrCoeffs',
    signature = signature(x = 'ExpressionSet'),
    function(x, transpose, use, method) {
        CorrCoeffs(Biobase::exprs(x), transpose, use, method)
    }
    )

setGeneric('HeatCorr',
    function(x, transpose = TRUE, use = 'everything',
             method = 'pearson', ...) {
        standardGeneric('HeatCorr')
    }
    )
setMethod('HeatCorr',
    signature = signature(x = 'matrix'),
    function(x, transpose, use, method, ...) {
        dat <- CorrCoeffs(x, transpose, use, method)
        HeatWrapCore(x = dat, dist = 'euclidean', linkage = 'average',
                     col = HeatColors(50), ...)
    }
    )

setMethod('HeatCorr',
    signature = signature(x = 'ExpressionSet'),
    function(x, transpose, use, method, ...) {
        HeatCorr(Biobase::exprs(x), transpose, use, method, ...)
    }
    )

setGeneric('BuildCorrNetwork',
    function(x, threshold = NA, transpose = TRUE, use = 'everything',
             method = 'pearson') {
        standardGeneric('BuildCorrNetwork')
    }
    )
setMethod('BuildCorrNetwork',
    signature = signature(x = 'matrix'),
    function(x, threshold, transpose, use, method) {
        coeffs <- CorrCoeffs(x, transpose, use, method)

        ## If threshold is not defined, default to using the median
        if (is.na(threshold))
            threshold = median(coeffs)

        ## Create a pseudo-dist matrix
        dat <- structure(
            ifelse(!lower.tri(coeffs), NA, coeffs),
            dimnames = list(rownames(coeffs), colnames(coeffs))
            )

        ## Make targets list for each element
        targets <- apply(dat, 1, function(row) {
            row <- row[!is.na(row)]
            data.frame(target = names(row), weight = row)
        })

        ## Final formatting
        plyr::ldply(mapply(function(source, targets) {
            dplyr::transmute(targets,
                source = source, target,
                correlation = ifelse(weight >= 0, '+', '-'),
                weight = abs(weight)
                ) %>%
            dplyr::filter(weight >= threshold)
        }, names(targets), targets, SIMPLIFY = FALSE, USE.NAMES = FALSE))
    }
    )

setMethod('BuildCorrNetwork',
    signature = signature(x = 'ExpressionSet'),
    function(x, threshold, transpose, use, method) {
        BuildCorrNetwork(Biobase::exprs(x), threshold, transpose, use, method)
    }
    )
